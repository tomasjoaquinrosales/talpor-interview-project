FROM python:3.7.8

WORKDIR /usr/src/app
COPY arepaspo/requirements.txt ./
RUN pip install -r requirements.txt
COPY arepaspo .
RUN python manage.py migrate
RUN python manage.py loaddata fixtures/fixture-20-12-2020.json

EXPOSE 8000
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
