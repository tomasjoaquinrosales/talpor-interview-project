# Talpor Interview Project

[Project Description](https://hackmd.io/iW40LhfbRSyDBBJVgwSQKQ?view)

## Task List

- [x] Create a user, order and order item model.
- [x] Create two end-points
- [x] Create a Dockerfile
- [x] Create unittest
- [x] Create a dumps of the db
- [x] Instruccions to run the application

## What kind of things you will find here ?

### Create a user, order and order item model.

To this task I created a django application call it arepaspo with the following folder structure:

```
arepaspo
├── arepaspo
│   └── ...
├── fixtures
├── sales
│   ├── api
│   │   ├── serializers.py
│   │   ├── responses.py        # the response file has the responsability to handler the logic
│   │   └── views.py            # the views file has the responsability to be a bridge between urls.py and the responses
|   ├── migrations
|   ├── tests
│   │   ├── tests_order_item.py
│   │   ├── tests_orders.py
│   │   └── tests_users.py
│   ├── admin.py
│   ├── apps.py
│   ├── models.py               # models
│   ├── urls.py                 # sales end-points
│   └── views.py
├── postman
│   └── ....
├── scripts
│   └── ....
├── .gitlab-ci.yml              # Continuos Integrations only to testing and build
└── Dockerfile                  # Dockerfile to deploy the application locally without dependecies

```

**The User, Order and OrderItem model are in arepaspo/sales/models.py**

### Notes Seccion

I've added an extra end-points to the task:

1. CREATE [POST] http://localhost:8000/api/users/
1. RETRIEVE [GET] http://localhost:8000/api/users/USER_ID/
1. LISTS [GET] http://localhost:8000/api/users/
1. CREATE [POST] http://localhost:8000/api/orders/
1. UPDATE [PUT] http://localhost:8000/api/orders/ORDER_ID/close/
1. UPDATE [PUT] http://localhost:8000/api/orders/ORDER_ID/reject/
1. CREATE [POST] http://localhost:8000/api/order-item/
1. LISTS [GET] http://localhost:8000/api/order-item/

### Create two end-points

**[GET] http://localhost:8000/api/orders/1/**

Response
```json
{
    "date": "2020-12-19",
    "status": "proccessed",
    "user_first_name": "talpor 1",
    "user_last_name": "talpor 1",
    "user_email": "user1@talpor.com",
    "total": "680.00",
    "total_items_purchased": 70,
    "items": [
        {
            "id": 1,
            "description": "arepa 1",
            "quantity": 15,
            "price": "10.00"
        },
        {
            "id": 2,
            "description": "arepa 5",
            "quantity": 15,
            "price": "10.00"
        },
        {
            "id": 3,
            "description": "arepa 4",
            "quantity": 15,
            "price": "10.00"
        },
        {
            "id": 4,
            "description": "arepa 3",
            "quantity": 15,
            "price": "10.00"
        },
        {
            "id": 7,
            "description": "arepa 6",
            "quantity": 10,
            "price": "8.00"
        }
    ]
}
```


**[GET] http://localhost:8000/api/orders/?status=active**

Response
```json
[
    {
        "date": "2020-12-20",
        "status": "active",
        "user_first_name": "talpor 2",
        "user_last_name": "talpor 2",
        "user_email": "user2@talpor.com",
        "total": "122.00",
        "total_items_purchased": 16
    },
    {
        "date": "2020-12-19",
        "status": "active",
        "user_first_name": "talpor 2",
        "user_last_name": "talpor 2",
        "user_email": "user2@talpor.com",
        "total": "230.00",
        "total_items_purchased": 25
    }
]
```

**[GET] http://localhost:8000/api/orders/?date=2020-12-20&status=active**

Response
```json
[
    {
        "date": "2020-12-20",
        "status": "active",
        "user_first_name": "talpor 2",
        "user_last_name": "talpor 2",
        "user_email": "user2@talpor.com",
        "total": "122.00",
        "total_items_purchased": 16
    }
]
```

To change the state of the order you could use the next end-point:

**[PUT] http://localhost:8000/api/orders/1/close/**


**[GET] http://localhost:8000/api/orders/?status=proccessed**

```json
[
    {
        "date": "2020-12-19",
        "status": "proccessed",
        "user_first_name": "talpor 1",
        "user_last_name": "talpor 1",
        "user_email": "user1@talpor.com",
        "total": "680.00",
        "total_items_purchased": 70
    }
]
```

**[GET] http://localhost:8000/api/orders/?total=25&status=active**

Response
```json
[
    {
        "date": "2020-12-19",
        "status": "active",
        "user_first_name": "talpor 2",
        "user_last_name": "talpor 2",
        "user_email": "user2@talpor.com",
        "total": "230.00",
        "total_items_purchased": 25
    }
]
```


Order status

| status |
| ------ |
| pending |
| proccessed |
| rejected |
| active   |


## Create Dockerfile

I've created a very simple Dockerfile to run quickly and without dependecies the django server. Here the step by step to run the Docker and the server:

1. Go to root Project / where is the Dockerfile
1. Start running Docker
1. Execute the following command to build the docker image: `docker build -t thomasdocker92/talpor .`. Could be another name.
1. Execute the next command to run the docker image saved locally: `docker run --rm  -it --name talpor -p 8000:8000 thomasdocker92/talpor`.

You will see the next screens:

![](dockerbuild.PNG)

![](dockerrun.PNG)


### Create unittest.

For this task I've created a folder into sales with the name *tests*, I tryed to test the most important end-points to get the high coverage possible.
In another hand, I've prepared a CI-CD file into this repository to simulate a deployment, for that you can go to CI / CD and watch the build and code quality stage. Maybe the code  quality is the most importat because has the result of run the test, for example this one:

![](cicd.PNG)

In the rigth side of the windows you can find the next view to see the coverage html report online:

![](artifacts.PNG)

### Create dumpfile

In the folder arepaspo/fixtures/ you will find the next file: _fixture-20-12-2020.json_ with my last sets of data. Be carefoul wiht this because I changed the enconding of the file.


### Instruccions to run the application

There are two ways to run the application, one of them its the docker way that I've described above, and the seconds is run with python:

1. Got to arepaspo folder
1. Create a environment file. For example:` mkvirtualenv C:\\<PATH_TO_AREPASPO_FOLDER>\venv`
1. Activate the environment: `venv/Script/activate` on windows or `source venv/bin/activate` on linux
1. Run the next command to install python dependecies: `pip install -r requirements.txt`
1. Run migrtions: `python manage.py migrate`
1. Load fixtures: `python manage.py loaddata fixtures\fixture-20-12-2020.json`
1. And then run: `python manage.py runserver localhost:8000`
1. Open a google windows and enter to http://localhost:8000 

