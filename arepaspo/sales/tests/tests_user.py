# Django
from django.test import TestCase

# Python
import json

# Django Rest Framework
from rest_framework.test import APIClient
from rest_framework import status


# Sales Models
from sales.models import User, Order, OrderItem
from sales.api.serializers import OrderSerializer


class UserTestCase(TestCase):
    def setUp(self):
        user = User(
            email="customer1@arepas.com",
            first_name="Customer 1",
            last_name="Customer 1",
        )
        user_2 = User(
            email="customer2@arepas.com",
            first_name="Customer 2",
            last_name="Customer 2",
        )
        user_3 = User(
            email="customer3@arepas.com",
            first_name="Customer 3",
            last_name="Customer 3",
        )
        user.save()
        user_2.save()
        user_3.save()

    def test_create_user(self):
        client = APIClient()

        user = {
            "first_name": "talpor 4",
            "last_name": "talpor 4",
            "email": "user4@talpor.com",
        }

        response = client.post("/api/users/", user, format="json")
        result = json.loads(response.content)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIn("id", result)
        self.assertIn("first_name", result)
        self.assertIn("last_name", result)
        self.assertIn("email", result)

    def test_create_invalid_user(self):
        client = APIClient()

        user = {
            "email": "user4@talpor.com",
        }

        response = client.post("/api/users/", user, format="json")
        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_get_users(self):
        client = APIClient()

        response = client.get("/api/users/")
        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(result), 3)

    def test_get_user(self):
        client = APIClient()

        response = client.get("/api/users/1/")
        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn("id", result)
        self.assertIn("first_name", result)
        self.assertIn("last_name", result)
        self.assertIn("email", result)

    def test_get_invald_user(self):
        client = APIClient()

        response = client.get("/api/users/5/")
        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
