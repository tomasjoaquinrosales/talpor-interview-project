# Django
from django.test import TestCase

# Python
import json

# Django Rest Framework
from rest_framework.test import APIClient
from rest_framework import status


# Sales Models
from sales.models import User, Order, OrderItem
from sales.api.serializers import OrderSerializer


class OrderTestCase(TestCase):
    def setUp(self):
        user = User(
            email="customer1@arepas.com",
            first_name="Customer 1",
            last_name="Customer 1",
        )
        user_2 = User(
            email="customer2@arepas.com",
            first_name="Customer 2",
            last_name="Customer 2",
        )
        user_3 = User(
            email="customer3@arepas.com",
            first_name="Customer 3",
            last_name="Customer 3",
        )
        user.save()
        user_2.save()
        user_3.save()

    def test_create_order(self):
        client = APIClient()
        customer_1 = User.objects.get(email="customer1@arepas.com")

        order = Order.objects.create(user=customer_1)

        order_item = {
            "order": order.id,
            "description": "arepa x",
            "price": 156.00,
            "quantity": 100,
        }

        response = client.post("/api/order-item/", order_item, format="json")
        result = json.loads(response.content)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIn("id", result)
        self.assertIn("description", result)
        self.assertIn("price", result)
        self.assertIn("quantity", result)
        self.assertEqual(result.get("price"), "156.00")

    def test_create_invalid_order(self):
        client = APIClient()
        customer_1 = User.objects.get(email="customer1@arepas.com")

        order = Order.objects.create(user=customer_1)

        order_item = {
            "order": order.id,
        }

        response = client.post("/api/order-item/", order_item, format="json")
        result = json.loads(response.content)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_get_order_items(self):
        client = APIClient()
        customer_1 = User.objects.get(email="customer1@arepas.com")
        order = Order.objects.create(user=customer_1)
        order_item = OrderItem.objects.create(
            order=order, description="arepa x", price=15.00, quantity=10
        )
        order_item = OrderItem.objects.create(
            order=order, description="arepa y", price=10.00, quantity=5
        )

        response = client.get("/api/order-item/")
        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(result), 2)
