# Django
from django.test import TestCase

# Python
import json
from datetime import date

# Django Rest Framework
from rest_framework.test import APIClient
from rest_framework import status


# Sales Models
from sales.models import User, Order, OrderItem
from sales.api.serializers import OrderSerializer


class OrderTestCase(TestCase):
    def setUp(self):
        user = User(
            email="customer1@arepas.com",
            first_name="Customer 1",
            last_name="Customer 1",
        )
        user_2 = User(
            email="customer2@arepas.com",
            first_name="Customer 2",
            last_name="Customer 2",
        )
        user_3 = User(
            email="customer3@arepas.com",
            first_name="Customer 3",
            last_name="Customer 3",
        )
        user.save()
        user_2.save()
        user_3.save()

    def test_create_order(self):
        client = APIClient()
        customer_3 = User.objects.get(email="customer3@arepas.com")

        order = {
            "user": customer_3.id,
        }

        response = client.post("/api/orders/", order, format="json")
        result = json.loads(response.content)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIn("id", result)
        self.assertIn("date", result)
        self.assertIn("status", result)
        self.assertIn("user", result)

    def test_create_order_with_invalid_user(self):
        client = APIClient()
        customer_3 = User.objects.get(email="customer3@arepas.com")

        order = {
            "user": 4,
        }

        response = client.post("/api/orders/", order, format="json")
        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn("user", result)

    def test_get_order_without_order_items(self):
        client = APIClient()

        customer_1 = User.objects.get(email="customer1@arepas.com")
        customer_2 = User.objects.get(email="customer2@arepas.com")

        order_1 = Order.objects.create(user=customer_1)
        order_2 = Order.objects.create(user=customer_2)
        response = client.get(f"/api/orders/{order_1.id}/")
        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertJSONEqual(response.content, "{}")

    def test_get_order_with_order_items(self):
        client = APIClient()

        customer_1 = User.objects.get(email="customer1@arepas.com")
        customer_2 = User.objects.get(email="customer2@arepas.com")

        order_1 = Order.objects.create(user=customer_1)
        order_2 = Order.objects.create(user=customer_2)

        order_item_1 = OrderItem.objects.create(
            order=order_1, description="arepa 1", quantity=5, price=35
        )
        order_item_2 = OrderItem.objects.create(
            order=order_1, description="arepa 2", quantity=15, price=10
        )

        response = client.get(f"/api/orders/{order_1.id}/")
        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn("325.00", result.get("total"))

    def test_get_invalid_order(self):
        client = APIClient()

        response = client.get(f"/api/orders/100/")
        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_whole_orders(self):
        client = APIClient()

        customer_1 = User.objects.get(email="customer1@arepas.com")
        customer_2 = User.objects.get(email="customer2@arepas.com")

        order_1 = Order.objects.create(user=customer_1)
        order_2 = Order.objects.create(user=customer_2)

        order_item_1 = OrderItem.objects.create(
            order=order_1, description="arepa 1", quantity=5, price=35
        )
        order_item_2 = OrderItem.objects.create(
            order=order_1, description="arepa 2", quantity=15, price=10
        )
        order_item_3 = OrderItem.objects.create(
            order=order_2, description="arepa 3", quantity=5, price=35
        )

        response = client.get(f"/api/orders/")
        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(result), 2)

    def test_get_orders_with_filters(self):
        client = APIClient()
        td = date.today().strftime("%Y-%m-%d")

        customer_1 = User.objects.get(email="customer1@arepas.com")
        customer_2 = User.objects.get(email="customer2@arepas.com")

        order_1 = Order.objects.create(user=customer_1, date=td)
        order_2 = Order.objects.create(user=customer_2, date=td)

        order_item_1 = OrderItem.objects.create(
            order=order_1, description="arepa 1", quantity=5, price=35
        )
        order_item_2 = OrderItem.objects.create(
            order=order_1, description="arepa 2", quantity=15, price=10
        )
        order_item_3 = OrderItem.objects.create(
            order=order_2, description="arepa 3", quantity=5, price=35
        )

        response = client.get(f"/api/orders/?date={td}")
        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(result), 2)

        response = client.get(f"/api/orders/?date=2020-12-18")
        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(result), 0)

        response = client.get(f"/api/orders/?date={td}&status=active")
        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(result), 2)

        response = client.get(f"/api/orders/?date={td}&status=pending")
        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(result), 0)

        response = client.get(f"/api/orders/?date={td}&status=active&total=5")
        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(result), 1)

    def test_close_order(self):
        client = APIClient()

        customer_1 = User.objects.get(email="customer1@arepas.com")

        order_1 = Order.objects.create(user=customer_1)

        order_item_1 = OrderItem.objects.create(
            order=order_1, description="arepa 1", quantity=5, price=35
        )
        order_item_2 = OrderItem.objects.create(
            order=order_1, description="arepa 2", quantity=15, price=10
        )

        response = client.put(f"/api/orders/{order_1.id}/close/", {}, format="json")
        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn("proccessed", result.get("status"))

    def test_close_invalid_order(self):
        client = APIClient()

        response = client.put(f"/api/orders/100/close/", {}, format="json")
        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_reject_order(self):
        client = APIClient()

        customer_1 = User.objects.get(email="customer1@arepas.com")

        order_1 = Order.objects.create(user=customer_1)

        order_item_1 = OrderItem.objects.create(
            order=order_1, description="arepa 1", quantity=5, price=35
        )
        order_item_2 = OrderItem.objects.create(
            order=order_1, description="arepa 2", quantity=15, price=10
        )

        response = client.put(f"/api/orders/{order_1.id}/reject/", {}, format="json")
        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn("rejected", result.get("status"))

    def test_reject_invalid_order(self):
        client = APIClient()

        response = client.put(f"/api/orders/100/reject/", {}, format="json")
        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
