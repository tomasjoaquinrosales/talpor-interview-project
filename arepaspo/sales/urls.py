from django.urls import path
from .api.views import OrderViewSet, OrderItemViewSet, UserViewSet
from rest_framework.urlpatterns import format_suffix_patterns


user_list = UserViewSet.as_view(
    {
        "get": "list",
        "post": "create",
    }
)

user_detail = UserViewSet.as_view(
    {"get": "retrieve", "patch": "partial_update", "delete": "destroy"}
)


order_list = OrderViewSet.as_view(
    {
        "get": "list",
        "post": "create",
    }
)

order_detail = OrderViewSet.as_view(
    {"get": "retrieve", "patch": "partial_update", "delete": "destroy"}
)

order_item_list = OrderItemViewSet.as_view(
    {
        "get": "list",
        "post": "create",
    }
)

order_close = OrderViewSet.as_view({"put": "close"})

order_reject = OrderViewSet.as_view({"put": "reject"})


urlpatterns = [
    path("users/", user_list, name="user-list"),
    path("users/<int:user_pk>/", user_detail, name="user-detail"),
    path("orders/", order_list, name="order-list"),
    path("orders/<int:order_pk>/", order_detail, name="order-detail"),
    path("orders/<int:order_pk>/close/", order_close, name="order-close"),
    path("orders/<int:order_pk>/reject/", order_reject, name="order-reject"),
    path("order-item/", order_item_list, name="order-item-list"),
]
