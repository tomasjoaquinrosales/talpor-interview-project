from django.db import models
from django.utils import timezone


class User(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    email = models.EmailField(unique=True)

    def __str__(self):
        return f"{self.first_name}, {self.last_name}, {self.email}"


class Order(models.Model):
    PENDING = "pending"
    PROCESSED = "proccessed"
    REJECTED = "rejected"
    ACTIVE = "active"
    ORDER_STATUS_CHOICES = [
        (PENDING, PENDING),
        (PROCESSED, PROCESSED),
        (REJECTED, REJECTED),
        (ACTIVE, ACTIVE),
    ]
    date = models.DateField(default=timezone.now().date())
    status = models.CharField(
        max_length=10, choices=ORDER_STATUS_CHOICES, default=ACTIVE
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.date} - {self.status}"

    def is_active(self):
        return self.status in {self.ACTIVE}

    def is_pending(self):
        return self.status in {self.PENDING}

    def is_processed(self):
        return self.status in {self.PROCESSED}

    def is_rejected(self):
        return self.status in {self.REJECTED}


class OrderItem(models.Model):
    description = models.CharField(max_length=250)
    quantity = models.IntegerField()
    price = models.DecimalField(max_digits=5, decimal_places=2)
    order = models.ForeignKey(Order, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.description}"
