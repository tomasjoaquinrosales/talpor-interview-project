from typing import List, Tuple, Dict
from rest_framework import status
from rest_framework.exceptions import ValidationError
from sales.models import User, Order, OrderItem
from .serializers import (
    UserSerializer,
    OrderSerializer,
    OrderItemSerializer,
    OrderItemListSerializer,
    OrderResponseSerializer,
)
from django.db.models import Sum, Count, F, Value, FloatField
from datetime import datetime


class UserResponse:
    def __init__(self, request):
        self.request = request

    def create(self):
        """Create a new user

        Return: A tuple with the data serialized or a error and the status code
        """

        serializer = UserSerializer(data=self.request.data)
        if serializer.is_valid():
            serializer.save()
            return serializer.data, status.HTTP_201_CREATED
        return serializer.errors, status.HTTP_400_BAD_REQUEST

    def get_users(self):
        """Gets each user of the system.

        Return: a list of users and http status 200
        """

        queryset = User.objects.all()
        serializer = UserSerializer(queryset, many=True)
        return serializer.data, status.HTTP_200_OK

    def get_user_by_id(self, user_pk):
        """Gets user filterd by id

        Keyword arguments:
        user_pk -- user primary key or identifier number
        Return: user serializer or an error if the user_pk is incorrect and status code 200
        """

        try:
            user = User.objects.get(pk=user_pk)
            serializer = UserSerializer(user)
            return serializer.data, status.HTTP_200_OK
        except User.DoesNotExist as e:
            return {
                "error": f"User({user_pk}) does not exists."
            }, status.HTTP_404_NOT_FOUND


class OrderResponse:
    def __init__(self, request):
        self.request = request

    def _set_order_response(self, order, items=None):
        """Sets a dictionary based in an order and a list of items

        Keyword arguments:
        order -- order object from db
        items -- list of items object from db
        Return: dict
        """

        order_dict = dict()
        order_dict["date"] = order.get("order__date")
        order_dict["status"] = order.get("order__status")
        order_dict["user_first_name"] = order.get("order__user__first_name")
        order_dict["user_last_name"] = order.get("order__user__last_name")
        order_dict["user_email"] = order.get("order__user__email")
        order_dict["total"] = order.get("final_price")
        order_dict["total_items_purchased"] = order.get("total_items")
        if items:
            order_dict["items"] = items
        return order_dict

    def get_orders(self):
        """Gets each order that match with the filters if exists.

        Return: list order serialized and status code
        """
        filter = dict()
        filter_annotation = dict()

        # GET QUERY PARAMS
        filter_date = self.request.query_params.get("date", None)
        filter_status = self.request.query_params.get("status", None)
        filter_total = self.request.query_params.get("total", None)

        # PREPARE FILTER
        if filter_date:
            filter["order__date"] = datetime.strptime(filter_date, "%Y-%m-%d").date()
        if filter_status:
            filter["order__status"] = filter_status.lower()
        if filter_total:
            filter_annotation["total_items"] = filter_total

        # GET ORDERS BY FILTERS
        queryset = (
            OrderItem.objects.order_by("-order__id")
            .filter(**filter)
            .values(
                "order__date",
                "order__status",
                "order__user__first_name",
                "order__user__last_name",
                "order__user__email",
            )
            .annotate(
                final_price=Sum(F("quantity") * F("price"), output_field=FloatField()),
                total_items=Sum("quantity"),
            )
            .filter(**filter_annotation)
        )

        # PREPARE RESPONSE
        response = list()
        for order in queryset:
            response.append(self._set_order_response(order))

        serializer = OrderResponseSerializer(response, many=True)
        return serializer.data, status.HTTP_200_OK

    def get_order_by_id(self, order_pk):
        """Gets order filterd by id.

        Keyword arguments:
        order_pk -- order primary key or identifier number
        Return: order serialized and status code
        """

        try:
            order = Order.objects.get(pk=order_pk)
        except Order.DoesNotExist as e:
            return {
                "error": f"Order({order_pk}) does not exists."
            }, status.HTTP_404_NOT_FOUND

        # ORDER
        queryset = (
            OrderItem.objects.order_by("-order__id")
            .filter(order__pk=order.pk)
            .values(
                "order__date",
                "order__status",
                "order__user__first_name",
                "order__user__last_name",
                "order__user__email",
            )
            .annotate(
                final_price=Sum(F("quantity") * F("price"), output_field=FloatField()),
                total_items=Sum("quantity"),
            )
        )

        # ORDER ITEMS
        order_item_list = OrderItem.objects.filter(order__pk=order.pk).values(
            "id", "description", "quantity", "price"
        )

        # PREPARE RESPONSE
        response = dict()
        for order in queryset:
            response = self._set_order_response(order, order_item_list)

        if not response:
            return {}, status.HTTP_200_OK

        serializer = OrderResponseSerializer(response, many=False)
        return serializer.data, status.HTTP_200_OK

    def create(self):
        """Creates a new order

        Keyword arguments:
        Return: order serialized or an error and status code
        """

        serializer = OrderSerializer(data=self.request.data)
        if serializer.is_valid():
            serializer.save()
            return serializer.data, status.HTTP_201_CREATED
        return serializer.errors, status.HTTP_400_BAD_REQUEST

    def close(self, order_pk):
        try:
            order = Order.objects.get(pk=order_pk)
        except Order.DoesNotExist as e:
            return {
                "error": f"Order({order_pk}) does not exists."
            }, status.HTTP_404_NOT_FOUND
        order.status = Order.PROCESSED
        order.save()
        serializer = OrderSerializer(order)
        return serializer.data, status.HTTP_200_OK

    def reject(self, order_pk):
        try:
            order = Order.objects.get(pk=order_pk)
        except Order.DoesNotExist as e:
            return {
                "error": f"Order({order_pk}) does not exists."
            }, status.HTTP_404_NOT_FOUND
        order.status = Order.REJECTED
        serializer = OrderSerializer(order)
        return serializer.data, status.HTTP_200_OK


class OrderItemResponse:
    def __init__(self, request):
        self.request = request

    def get_order_items(self):
        """Gets each order item

        Return: order list serialized
        """

        queryset = OrderItem.objects.all()
        serializer = OrderItemListSerializer(queryset, many=True)
        return serializer.data, status.HTTP_200_OK

    def create(self):
        """Creats an new order item to a order

        Return: order item serialized or an error and status code
        """

        serializer = OrderItemSerializer(data=self.request.data)
        if serializer.is_valid():
            serializer.save()
            return serializer.data, status.HTTP_201_CREATED
        return serializer.errors, status.HTTP_400_BAD_REQUEST