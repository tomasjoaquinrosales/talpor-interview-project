from django.http.response import JsonResponse
from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.response import Response
from .responses import UserResponse, OrderResponse, OrderItemResponse
from rest_framework import status


class UserViewSet(viewsets.ViewSet):
    def list(self, request):
        response = UserResponse(request=request)
        users, status_code = response.get_users()
        return JsonResponse(data=users, status=status_code, safe=False)

    def retrieve(self, request, user_pk=None):
        response = UserResponse(request=request)
        user, status_code = response.get_user_by_id(user_pk)
        return JsonResponse(data=user, status=status_code, safe=False)

    def create(self, request):
        response = UserResponse(request=request)
        user, status_code = response.create()
        return JsonResponse(data=user, status=status_code, safe=False)

    def partial_update(self, request, user_pk=None):
        """Not Implemented"""
        return JsonResponse(
            data={"Error": "Method Not Implemented"}, status=status.HTTP_404_NOT_FOUND
        )

    def destroy(self, request, user_pk=None):
        """Not Implemented"""
        return JsonResponse(
            data={"Error": "Method Not Implemented"}, status=status.HTTP_404_NOT_FOUND
        )


class OrderViewSet(viewsets.ViewSet):
    def list(self, request):
        response = OrderResponse(request=request)
        orders, status_code = response.get_orders()
        return JsonResponse(data=orders, status=status_code, safe=False)

    def retrieve(self, request, order_pk=None):
        response = OrderResponse(request=request)
        order, status_code = response.get_order_by_id(order_pk)
        return JsonResponse(data=order, status=status_code, safe=False)

    def create(self, request):
        response = OrderResponse(request=request)
        users, status_code = response.create()
        return JsonResponse(data=users, status=status_code, safe=False)

    def close(self, request, order_pk=None):
        response = OrderResponse(request=request)
        order, status_code = response.close(order_pk)
        return JsonResponse(data=order, status=status_code, safe=False)

    def reject(self, request, order_pk=None):
        response = OrderResponse(request=request)
        order, status_code = response.reject(order_pk)
        return JsonResponse(data=order, status=status_code, safe=False)

    def partial_update(self, request, order_pk=None):
        """Not Implemented"""
        return JsonResponse(
            data={"Error": "Method Not Implemented"}, status=status.HTTP_404_NOT_FOUND
        )

    def destroy(self, request, order_pk=None):
        """Not Implemented"""
        return JsonResponse(
            data={"Error": "Method Not Implemented"}, status=status.HTTP_404_NOT_FOUND
        )


class OrderItemViewSet(viewsets.ViewSet):
    def list(self, request):
        response = OrderItemResponse(request=request)
        users, status_code = response.get_order_items()
        return JsonResponse(data=users, status=status_code, safe=False)

    def create(self, request):
        response = OrderItemResponse(request=request)
        users, status_code = response.create()
        return JsonResponse(data=users, status=status_code, safe=False)
