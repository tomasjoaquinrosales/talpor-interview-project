from rest_framework import serializers
from sales.models import User, Order, OrderItem


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["id", "first_name", "last_name", "email"]


class OrderListSerializer(serializers.ModelSerializer):
    user = UserSerializer(many=False, read_only=True)

    class Meta:
        model = Order
        fields = ["id", "date", "status", "user"]


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = ["id", "date", "status", "user"]


class OrderItemListSerializer(serializers.ModelSerializer):
    order = OrderSerializer(many=False, read_only=True)

    class Meta:
        model = OrderItem
        fields = ["id", "description", "quantity", "price", "order"]


class OrderItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderItem
        fields = ["id", "description", "quantity", "price", "order"]

    def validate_order(self, value):
        """
        Check that the order is active/proccessed.
        """

        if value.status in [Order.PROCESSED, Order.REJECTED, Order.PENDING]:
            raise serializers.ValidationError("This order already was proccessed.")
        return value


class OrderItemChildSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderItem
        fields = ["id", "description", "quantity", "price"]


class OrderResponseSerializer(serializers.Serializer):
    date = serializers.DateField()
    status = serializers.CharField()
    user_first_name = serializers.CharField()
    user_last_name = serializers.CharField()
    user_email = serializers.EmailField()
    total = serializers.DecimalField(max_digits=5, decimal_places=2)
    total_items_purchased = serializers.IntegerField()
    items = OrderItemChildSerializer(many=True, required=False)
